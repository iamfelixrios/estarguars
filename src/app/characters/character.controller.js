(function() {
  'use strict';

  angular
    .module('estarguars')
    .controller('CharacterController', characterController);

  /** @ngInject */
  function characterController($timeout, $http, endpoint, toastr, $scope, $modal) {
    $scope.pageClass = "page-char";
    var vm = this;
    vm.classAnimation = '';
    vm.showToastr = showToastr;
    vm.next = '';
    vm.characters = [];
    vm.getData = getData;
    vm.getNumArr = getNumArr;
    vm.notEmpty = notEmpty;
    vm.details = details;

    getData();

    function details(item) {
      $scope.char = item;
      $scope.close = function() { modalInstance.close(); };
      var modalInstance = $modal.open({
        templateUrl: 'app/characters/details.html',
        size: 'sm',
        scope: $scope
      });
    }

    function getNumArr(num) {
      return new Array(num);
    }

    function getData() {
      vm.loading = true;

      var calledOne;

      if (vm.next == '')
        calledOne = endpoint + 'people/';
      else if (vm.next)
        calledOne = vm.next;

      if (calledOne)
        $http
          .get(calledOne)
          .then(function (res) {
            vm.characters = vm.characters.concat(res.data.results);
            vm.next = res.data.next;
            console.log(vm.characters);
            vm.loading = false;
          }, function(err) {
            showError('Error calling Star Wars API');
            vm.loading = false;
          });

    }

    function notEmpty(str) {
      return (str != 'n/a' && str != 'unknown' && str != "0" && str != 'none');
    }

    function showError(err) {
      toastr.error(err);
      vm.classAnimation = '';
    }
    function showToastr(msg) {
      toastr.info(msg);
      vm.classAnimation = '';
    }

  }
})();
