(function() {
  'use strict';

  angular
    .module('estarguars')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastr, $authProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastr.options.timeOut = 3000;
    toastr.options.positionClass = 'toast-top-right';
    toastr.options.preventDuplicates = true;
    toastr.options.progressBar = true;

    // Login
    $authProvider.facebook({
      clientId: '952999758097844',
      url: 'http://localhost:8080/auth/facebook/',
      responseType: 'token'
    });
    $authProvider.twitter({
      url: 'http://localhost:8080/auth/twitter',
      authorizationEndpoint: 'https://api.twitter.com/oauth/authenticate',
      redirectUri: 'http://localhost:3000',
      type: '1.0',
      popupOptions: { width: 495, height: 645 }
    });
  }

})();
