/* global malarkey:false, toastr:false, moment:false */
(function() {
  'use strict';

  angular
    .module('estarguars')
    .constant('malarkey', malarkey)
    .constant('toastr', toastr)
    .constant('moment', moment)
    .constant('endpoint', 'http://swapi.co/api/');

})();
