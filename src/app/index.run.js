(function() {
  'use strict';

  angular
    .module('estarguars')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
