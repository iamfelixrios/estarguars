(function() {
  'use strict';

  angular
    .module('estarguars', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'satellizer']);

})();
