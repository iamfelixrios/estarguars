(function() {
  'use strict';

  angular
    .module('estarguars')
    .controller('VehicleController', vehicleController);

  /** @ngInject */
  function vehicleController($timeout, $http, endpoint, toastr, $scope, $modal) {
    $scope.pageClass = "page-char";
    var vm = this;
    vm.classAnimation = '';
    vm.showToastr = showToastr;
    vm.next = '';
    vm.vehicles = [];
    vm.getData = getData;
    vm.getNumArr = getNumArr;
    vm.getNumber = getNumber;
    vm.notEmpty = notEmpty;
    vm.details = details;

    getData();

    function details(item) {
      $scope.ship = item;
      $scope.close = function() { modalInstance.close(); };
      var modalInstance = $modal.open({
        templateUrl: 'app/vehicles/details.html',
        size: 'sm',
        scope: $scope
      });
    }

    function getNumArr(num) {
      return new Array(num);
    }

    function getData() {
      vm.loading = true;

      var calledOne;

      if (vm.next == '')
        calledOne = endpoint + 'vehicles/';
      else if (vm.next)
        calledOne = vm.next;

      if (calledOne)
        $http
          .get(calledOne)
          .then(function (res) {
            vm.vehicles = vm.vehicles.concat(res.data.results);
            vm.next = res.data.next;
            console.log(vm.vehicles);
            vm.loading = false;
          }, function(err) {
            showError('Error calling Star Wars API');
            vm.loading = false;
          });

    }

    function notEmpty(str) {
      return (str != 'n/a' && str != 'unknown' && str != "0" && str != 'none');
    }

    function getNumber(str) {
      var numb = str.match(/\d/g);

      if (numb) {
        numb = numb.join("");

        var result = parseInt(numb);

        if (result >= 1000000000000)
          return Math.round(result / 1000000000000) + 'B';
        else if (result >= 1000000)
          return Math.round(result / 1000000) + 'M';
        else if (result >= 1000)
          return Math.round(result / 1000) + 'K';
        else
          return result;
      } else {
        return '';
      }

    }

    function showError(err) {
      toastr.error(err);
      vm.classAnimation = '';
    }
    function showToastr(msg) {
      toastr.info(msg);
      vm.classAnimation = '';
    }

  }
})();
