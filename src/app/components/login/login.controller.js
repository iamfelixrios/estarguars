(function() {
    'use strict';

    angular
        .module('estarguars')
        .controller('LoginCtrl', loginCtrl);

    /** @ngInject */
    function loginCtrl($scope, $auth) {

        $scope.authenticate = function(provider) {
            $auth.authenticate(provider)
              .then(function(response) {
                console.log(response);
                $scope.$emit('login', response.data.name);

              })
        };


    }

})();
