(function() {
  'use strict';

  angular
    .module('estarguars')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController($auth, $scope) {
      var vm = this;

      $scope.$on('login', function (event, data) {
        localStorage.setItem('username', data);
        vm.username = data;
        vm.isAuthenticated = $auth.isAuthenticated();

      });

      vm.logout = function() {
        vm.isAuthenticated = false;
        vm.username = undefined;
      };

      vm.username = localStorage.getItem('username');

      // "vm.creation" is avaible by directive option "bindToController: true"
      vm.relativeDate = moment(vm.creationDate).fromNow();
      vm.isAuthenticated = $auth.isAuthenticated();
    }
  }

})();
