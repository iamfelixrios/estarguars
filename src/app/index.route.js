(function() {
  'use strict';

  angular
      .module('estarguars')
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
          url: '/',
          templateUrl: 'app/main/main.html',
          controller: 'MainController',
          controllerAs: 'main'
        })
        .state('about', {
          url: '/about',
          templateUrl: 'app/main/about.html',
          controller: 'AboutController',
          controllerAs: 'about'
        })
        .state('characters', {
          url: '/characters',
          templateUrl: 'app/characters/characters.html',
          controller: 'CharacterController',
          controllerAs: 'vm'
        })
        .state('starships', {
          url: '/starships',
          templateUrl: 'app/starships/starships.html',
          controller: 'StarshipController',
          controllerAs: 'vm'
        })
        .state('vehicles', {
            url: '/vehicles',
            templateUrl: 'app/vehicles/vehicles.html',
            controller: 'VehicleController',
            controllerAs: 'vm'
          });

    $urlRouterProvider.otherwise('/');
  }

})();
