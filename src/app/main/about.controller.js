(function() {
  'use strict';

  angular
    .module('estarguars')
    .controller('AboutController', aboutController);

  /** @ngInject */
  function aboutController($scope) {
      $scope.pageClass = "page-about";
  }
})();
